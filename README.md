## snowCrypto 

Discord bot that tells you the price of specified crypto in your specified currency every 5 minutes, as well as on command.

Usage: Set your discord bot token, Coinbase key, Coinbase secret, as well as your Coinbase wallet in `config.json`, then modify the variables in `main.js` to match your currencies. Then `npm start`.

There might be some code that limits command issuing to certain discord IDs, feel free to remove that because I won't.
