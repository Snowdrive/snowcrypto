const Embeds = require('./../utils/embeds');
const Logger = require('../utils/logger');

exports.name = 'echo';
exports.description = 'Echoes what you say';
// eslint-disable-next-line no-unused-vars
exports.exec = function(ethBalance, message, THRESHOLD, commandArgs) {
  try {
    if(message.author.id !== '217374203576451072' && message.author.id !== '422079143782383618') {
      message.channel.send('⚠ You are not authorized to perform this action. ⚠');
      return;
    }
    if(message.author.bot) return;
    message.delete();
    message.channel.send(commandArgs);
  }
  catch (error) {
    Logger.error(`Caught error while executing '${this.name}' command: ${error}`);
    message.channel.send(Embeds.error());
  }
};