const Embeds = require('./../utils/embeds');
const Logger = require('../utils/logger');
const { getPrice } = require('../utils/utils');

exports.name = 'summary';
exports.description = 'Summary of Ethereum';
exports.aliases = ['sum', 's'];

const config = require('../config');
const COINBASEKEY = config.coinbasekey;
const COINBASESECRET = config.coinbasesecret;
const COINBASEWALLET = config.coinbasewallet;

const lastBalance = 0;
const LOWERTHRESHOLD = 1500;
const LOWERTHRESHOLD2 = 1700;

const CoinbaseLibrary = require('coinbase-library');
const Coinbase = new CoinbaseLibrary(COINBASEKEY, COINBASESECRET);

// eslint-disable-next-line no-unused-vars
exports.exec = async function(ethBalance, message, THRESHOLD, commandArgs) {
  try {
    const msg = await message.channel.send('Loading...');
    const ethPrice = await getPrice();
    const balance = await Coinbase.getBalance(COINBASEWALLET, 'both');
    const eurBalance = balance.eur;
    msg.delete({ timeout: 1 });
    message.channel.send(Embeds.summary(ethPrice, eurBalance, lastBalance));
    if(ethPrice > THRESHOLD) {
      message.channel.send(`<@217374203576451072>, price of <:eth:846456903865073694> has surpassed the threshold of ${THRESHOLD} <:eur:846485904394944532>`);
    }
    else if (ethPrice < LOWERTHRESHOLD) {
      message.channel.send(`<@217374203576451072>, price is below threshold! (${LOWERTHRESHOLD}  <:eur:846485904394944532>)`);
    }
    else if (ethPrice < LOWERTHRESHOLD2) {
      message.channel.send(`<@217374203576451072>, price is below threshold! (${LOWERTHRESHOLD2}  <:eur:846485904394944532>)`);
    }
  }
  catch (error) {
    Logger.error(`Caught error while executing '${this.name}' command: ${error}`);
    message.channel.send(Embeds.error());
  }
};