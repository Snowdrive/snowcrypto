exports.name = 'eval';
exports.description = 'Not for your eyes 👁';
// eslint-disable-next-line no-unused-vars
exports.exec = async function(ethBalance, message, THRESHOLD, commandArgs) {

  const IS_ENABLED = false;

  if(message.author.id !== '217374203576451072' && message.author.id !== '422079143782383618') {
    message.channel.send('⚠ You are not authorized to perform this action. ⚠');
    return;
  }
  else if (!IS_ENABLED) {
    message.channel.send('⚠ This command is currently disabled. ⚠');
    return;
  }

  try {
    const result = await eval(`${commandArgs}`);
    message.channel.send(`:white_check_mark: Eval succeeded: \`\`\`js\n${commandArgs}\`\`\``);
    return message.channel.send(`**Result:** ${result}`);
  }
  catch (error) {
    console.error(`Caught error while executing 'eval' command: ${error}`);
    message.channel.send(`:x: Eval failed: \`\`\`js\n${commandArgs}\`\`\``);
    return message.channel.send(`**Error:** ${error}`);
  }
};