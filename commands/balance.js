const Embeds = require('./../utils/embeds');
const Logger = require('../utils/logger');
const { getPrice } = require('../utils/utils');

exports.name = 'balance';
exports.description = 'Shows your balance';
exports.aliases = ['bal', 'b'];
// eslint-disable-next-line no-unused-vars
exports.exec = async function(ethBalance, message, THRESHOLD, commandArgs) {
  try {
    const msg = await message.channel.send('Loading...');

    const ethPrice = await getPrice();
    const eurBalance = ethPrice * ethBalance;

    if(eurBalance > 25) {
      message.client.user.setStatus('online');
      message.client.user.setActivity('the profits', { type: 'WATCHING' });
    }
    else {
      message.client.user.setStatus('dnd');
      message.client.user.setActivity('the losses...', { type: 'WATCHING' });
    }
    msg.delete({ timeout: 1 });
    message.channel.send(Embeds.balance(ethBalance, eurBalance));
  }
  catch (error) {
    Logger.error(`Caught error while executing '${this.name}' command: ${error}`);
    message.channel.send(Embeds.error());
  }
};