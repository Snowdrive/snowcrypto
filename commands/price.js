const Embeds = require('./../utils/embeds');
const Logger = require('../utils/logger');
const { getPrice } = require('../utils/utils');

exports.name = 'price';
exports.description = 'Shows price of ETH';
exports.aliases = 'p';
// eslint-disable-next-line no-unused-vars
exports.exec = async function(ethBalance, message, THRESHOLD, commandArgs) {
  try {
    const msg = await message.channel.send('Loading...');
    const ethPrice = await getPrice();
    msg.delete({ timeout: 1 });
    message.channel.send(Embeds.price(ethPrice));
    if(ethPrice > THRESHOLD) {
      message.channel.send(`<@217374203576451072>, price of <:eth:846456903865073694> has surpassed the threshold of ${THRESHOLD} <:eur:846485904394944532>`);
    }
  }
  catch (error) {
    Logger.error(`Caught error while executing '${this.name}' command: ${error}`);
    message.channel.send(Embeds.error());
  }
};