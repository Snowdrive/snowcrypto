'use strict';
const Discord = require('discord.js');

const client = new Discord.Client();

const fs = require('fs');

const Embeds = require('./utils/embeds.js');
const Logger = require('./utils/logger.js');
const Vars = require('./vars.json');
const Commands = new Discord.Collection();
const config = require('./config');
const PREFIX = config.prefix;
const TOKEN = config.token;
const COINBASEKEY = config.coinbasekey;
const COINBASESECRET = config.coinbasesecret;
const COINBASEWALLET = config.coinbasewallet;

const CoinbaseLibrary = require('coinbase-library');
const Coinbase = new CoinbaseLibrary(COINBASEKEY, COINBASESECRET);

// array of minute times at which to send summary
const minArray = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60];
let lastBalance = 0;

// threshold of ETH at which to notify, and point at which it crosses losses/profits
const UPPERTHRESHOLD = Vars.upperthreshold;
const LOWERTHRESHOLD = Vars.lowerthreshold;
const TOTALINVESTED = Vars.totalinvested;

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
  const command = require(`./commands/${file}`);
  Commands.set(file.slice(0, -3), command);
}

client.on('ready', () => {
  console.log('Ready');
  // Time check every second
  // eslint-disable-next-line no-unused-vars
  const timer = setInterval(async function() {
    const date = new Date();

    if(minArray.includes(date.getMinutes()) && date.getSeconds() === 0) {
      const balance = await Coinbase.getBalance(COINBASEWALLET, 'both');
      const ethPrice = await Coinbase.getRate('ETH', 'EUR');
      const eurBalance = balance.eur;

      if(eurBalance > TOTALINVESTED) {
        client.user.setStatus('online');
        client.user.setActivity('the profits', { type: 'WATCHING' });
      }
      else {
        client.user.setStatus('dnd');
        client.user.setActivity('the losses...', { type: 'WATCHING' });
      }

      client.channels.cache.get('846393284314398723').send(Embeds.summary(ethPrice, eurBalance, lastBalance));
      lastBalance = eurBalance;
      if(ethPrice > UPPERTHRESHOLD) {
        client.channels.cache.get('846393284314398723').send(`<@217374203576451072>, price is above threshold! (${UPPERTHRESHOLD}  <:eur:846485904394944532>)`);
      }
      else if (ethPrice < LOWERTHRESHOLD) {
        client.channels.cache.get('846393284314398723').send(`<@217374203576451072>, price is below threshold! (${LOWERTHRESHOLD}  <:eur:846485904394944532>)`);
      }
    }
  }, 1000 * 1 * 1);
  client.user.setActivity('at the loading screen...', { type: 'WATCHING' });
  client.user.setStatus('idle');
  // online, idle, dnd, invisible
});

client.on('rateLimit', () => {
  Logger.error('Rate limited');
});

client.on('message', async (message) => {
  if(message.author.bot) return;

  if (!message.content.startsWith(PREFIX)) return;
  const input = message.content.slice(PREFIX.length).trim();
  if (!input.length) return;
  const [, commandInput, commandArgs] = input.match(/(\w+)\s*([\s\S]*)/);

  const command = Commands.get(commandInput) || Commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandInput));
  const commandName = (Commands.get(commandInput) && Commands.get(commandInput).name) ||
                        (Commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandInput)) && Commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandInput)).name);

  if(!command) return;

  if(message.author.id !== '217374203576451072' && message.author.id !== '422079143782383618') {
    message.channel.send('⚠ You are not authorized to perform this action. ⚠');
    return;
  }

  try {
    const balance = await Coinbase.getBalance(COINBASEWALLET, 'both');
    const ethBalance = balance.eth;
    Logger.cmd(`${message.author.tag} executed '${commandName}' command`);
    command.exec(ethBalance, message, UPPERTHRESHOLD, commandArgs);
  }
  catch(error) {
    Logger.error(`Caught error while executing '${commandName}' command: ${error}`);
    message.channel.send(Embeds.error());
  }

});

client.login(TOKEN);