const { MessageEmbed } = require('discord.js');
const Emoji = require('./emoji');
const Vars = require('../vars.json');

const TOTALINVESTED = Vars.totalinvested;

exports.error = function() {
  try {
    return (new MessageEmbed()
      .setColor('FF0000')
      .setTitle('Error occured'));
  }
  catch (error) {
    // TODO
  }
};


exports.balance = function(ethBalance, eurBalance) {
  try {
    const ethBalanceFloat = parseFloat(ethBalance);
    const eurBalanceFloat = parseFloat(eurBalance);

    return (new MessageEmbed()
      .setColor('0x5a6dce')
      .setTitle(`${Emoji.eur} Balance`)
      .setDescription(`*Your balance is ${ethBalanceFloat.toFixed(4)} ${Emoji.eth}, which is ${eurBalanceFloat.toFixed(2)} ${Emoji.eur}*`)
      .setFooter('Powered by Coinbase Library, graciously provided by TitasNxLT')
      .setURL('https://www.coinbase.com/price/ethereum'));
  }
  catch (error) {
    // TODO
  }
};

exports.summary = function(price, balance, lastBalance) {
  const time = new Date();
  const timeFormatted = time.toLocaleString();

  const balanceFloat = parseFloat(balance);
  const priceFloat = parseFloat(price);

  const PROFITS = balanceFloat - TOTALINVESTED;

  // TODO: HARDCODE SECONDS

  let changeSymbol = ':arrow_right:';

  if(lastBalance === 0) {
    changeSymbol = ':arrows_counterclockwise:';
  }
  else if(balance > lastBalance) {
    changeSymbol = ':arrow_upper_right:';
  }
  else if(balance < lastBalance) {
    changeSymbol = ':arrow_lower_right:';
  }

  return (new MessageEmbed()
    .setColor('0x5a6dce')
    .setTitle('Summary of Ethereum at ' + timeFormatted)
    .setDescription(`${Emoji.eth} *1 ETH = ${priceFloat.toFixed(2)} ${Emoji.eur}*\n\n${changeSymbol} *Balance: ${balanceFloat.toFixed(2)} ${Emoji.eur}*\n\n*:moneybag: Profits: ${PROFITS.toFixed(2)} ${Emoji.eur}*\n\n`)
    .setFooter('Powered by Coinbase Library, graciously provided by TitasNxLT')
    .setThumbnail('https://icons.iconarchive.com/icons/cjdowner/cryptocurrency-flat/1024/Ethereum-ETH-icon.png')
    .setURL('https://www.coinbase.com/price/ethereum'));
};

exports.price = function(price) {
  const time = new Date();
  const timeFormatted = time.toLocaleString();

  return (new MessageEmbed()
    .setColor('0x5a6dce')
    .setTitle('Price of Ethereum at ' + timeFormatted)
    .setDescription(`<:eth:846456903865073694> *1 ETH = ${price}* <:eur:846485904394944532>`)
    .setFooter('Powered by Coinbase Library, graciously provided by TitasNxLT')
    .setURL('https://www.coinbase.com/price/ethereum'));
};