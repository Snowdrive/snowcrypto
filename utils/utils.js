const config = require('../config');
const COINBASEKEY = config.coinbasekey;
const COINBASESECRET = config.coinbasesecret;

const CoinbaseLibrary = require('coinbase-library');
const Coinbase = new CoinbaseLibrary(COINBASEKEY, COINBASESECRET);

exports.getPrice = async function() {
  const returnedData = await Coinbase.getRate('ETH', 'EUR');
  return returnedData;
};